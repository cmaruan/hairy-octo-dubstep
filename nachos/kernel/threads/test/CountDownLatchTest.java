package nachos.kernel.threads.test;

import java.util.Random;

import nachos.Debug;
import nachos.kernel.Nachos;
import nachos.kernel.threads.CountDownLatch;
import nachos.machine.NachosThread;

/**
 * Demonstrates how CountDownLatch works.
 * It takes a given number of threads and share among them the same CountDownLatch.
 */
public class CountDownLatchTest implements Runnable {    
    /** Latch used for decrement */
    public CountDownLatch latch;
    
    
    /**
     * Initialize an instance of CountDownLatchTest
     * 
     * @param count initialize a CountDownLatch with that value
     * @param nthreads number of threads that will be created
     */
    public CountDownLatchTest(int count, int nthreads) {	
	latch = new CountDownLatch(count);
	for(int id = 0; id < nthreads; id++)
	    Nachos.scheduler.readyToRun(
		    	new NachosThread("CDLT thread " + id, this));
    }
    
    
    /**
     * Each thread waits till main thread decrement totally the latch. */
    @Override
    public void run() {
	Debug.println('1', NachosThread.currentThread().name + " started.");
	latch.await();
	Nachos.scheduler.finishThread();
    }
    
    /** Main thread calls the countdown method */
    public static void start() {
	CountDownLatchTest test = new CountDownLatchTest(4, 20);
	for (int i = 0; i < 4; i++) {
	    test.latch.countDown();
	    Nachos.scheduler.yieldThread();
	}
    }
}
