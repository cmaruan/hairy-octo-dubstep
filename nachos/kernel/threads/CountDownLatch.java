package nachos.kernel.threads;

import nachos.Debug;
import nachos.machine.NachosThread;

/**
 * A CountDownLatch is an object that provides a thread with the
 * ability to block until a specified number of other activities
 * have completed.  A CountDownLatch object is created with a specified
 * initial count.  A thread calling the await() method blocks within
 * the method call if the count is greater than zero.  Calls to the
 * countDown() method decrement the count.  When the count reaches zero,
 * any threads blocked within await() are awakened and allowed to proceed.
 * A CountDownLatch can only be used once; there is no facility for
 * resetting the count and starting over.
 */
public class CountDownLatch {    
    /** The value of the countdown latch */
    private int count;
    
    /** Semaphore used to male CountDown atomic */
    private Semaphore sem;
    
    /**
     * As Semaphores have implemented internally a waiting list, 
     * it can be used to lock all the threads until some condition
     * has been met */
    private Semaphore waitingThreads;
    /**
     * Initialize a CountDownLatch with a specified
     * initial count value.
     *
     * @param count  The initial count.
     */
    public CountDownLatch(int count) {
	sem = new Semaphore("CountDownLatch", 1);
	waitingThreads = new Semaphore("CountDownLatch Waiting List", 0);
	this.count = count;
	
	Debug.println('1', "CountDownLatch[" + NachosThread.currentThread().name + "]: new object. count = " + count);
    }

    /**
     * Decrement the count.  If the new count is less than or equal
     * to zero, any threads blocked within calls to await() are awakened
     * and allowed to proceed.
     */
    public void countDown() {
	Debug.println('1', "CountDownLatch[" + NachosThread.currentThread().name + "]: countDown:\tentering countDown");
	sem.P();
	count--;
	if (count == 0) {
	    // if count is equal or below zero, it frees the blocked threads 
	    Debug.println('1', "CountDownLatch[" + NachosThread.currentThread().name + "]: countDown:\tawaking threads");
	    waitingThreads.V();
	}
	sem.V();
	Debug.println('1', "CountDownLatch[" + NachosThread.currentThread().name + "]: countDown:\texiting countDown");
    }

    /**
     * When this method is called, if the count is greater than zero
     * the caller will block within the method call until the count
     * has been decremented to zero or less.  If the count is already
     * less than or equal to zero at the time await() is called, it
     * returns immediately.
     */
    public void await() {
	Debug.println('1', "CountDownLatch[" + NachosThread.currentThread().name + "]: await:\tentering await");
	sem.P();
	if (count <= 0) {
	    sem.V();
	    Debug.println('1', "CountDownLatch[" + NachosThread.currentThread().name + "]: await:\tcount <= 0. Returning");
	    return;
	}
	sem.V();
	// if count > 0 block the thread
	waitingThreads.P();
	Debug.println('1', "CountDownLatch[" + NachosThread.currentThread().name + "]: await:\tthread wating");
	// only reaches this part when count <= 0
	waitingThreads.V();
	Debug.println('1', "CountDownLatch[" + NachosThread.currentThread().name + "]: await:\tthread released");
    }

}