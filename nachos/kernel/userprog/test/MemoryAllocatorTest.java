package nachos.kernel.userprog.test;

import java.util.Random;

import nachos.Debug;
import nachos.kernel.Nachos;
import nachos.kernel.userprog.MemoryAllocator;
import nachos.machine.NachosThread;

public class MemoryAllocatorTest {
    
    private static final int N_THREADS = 20;
    private static final int MAX_ALLOCS_PER_THREAD = 100;
    private static final int MIN_ALLOCS_PER_THREAD = 20;
    private static final int WASTE_TIME = 10;
    private static final int PROCESSING_TIME = 100;
    
   
    
    public static void start() {
	final Random rand = new Random();
	MemoryAllocator.init();
	
	for(int i = 0; i < N_THREADS; i++) {
	    NachosThread t = new NachosThread("Thread " + i,
		    new Runnable() {
		        @Override
		        public void run() {
		            NachosThread self = NachosThread.currentThread();
		            int npages = rand.nextInt(MAX_ALLOCS_PER_THREAD - MIN_ALLOCS_PER_THREAD) + MIN_ALLOCS_PER_THREAD;
		            int yields = rand.nextInt(WASTE_TIME);
		            int processing = rand.nextInt(PROCESSING_TIME);
		            
		            
		            for (int j = 0; j < yields; j++)
		        	Nachos.scheduler.yieldThread();

		            Debug.println('2', self.name + " wants " + npages + " pages");
		            int[] pages = MemoryAllocator.allocatePages(npages);

		            Debug.println('2', self.name + " allocated its memory.. Processing");
		            for (int j = 0; j < processing; j++)
		        	Nachos.scheduler.yieldThread();
		            

		            Debug.println('2', self.name + " deallocates its memory (" + npages + " pages).");
		            MemoryAllocator.free(pages);
		            Debug.println('2', self.name + " finishing");
		            Nachos.scheduler.finishThread();
		        }
		    });
	    Nachos.scheduler.readyToRun(t);
	}
	
    }
}
