package nachos.kernel.userprog;

import java.util.ArrayList;

import nachos.Debug;
import nachos.kernel.threads.Condition;
import nachos.kernel.threads.Lock;
import nachos.kernel.threads.SpinLock;
import nachos.machine.Machine;
import nachos.machine.NachosThread;
/**
 * This class is responsible for allocating memory pages.
 * This is done by using a stack with all the current free
 * pages available. If a page is not there, it's being used. 
 * The complexity of allocating/freeing memory is O(1),
 * because it is reduced to simply pushing into or 
 * popping from a stack.
 * This method should be improved, once the operating 
 * system should know which process is using each page.
 * */
public class MemoryAllocator {
    /** Keeps track of which pages are free to be allocated. */
    private static ArrayList<Integer> freePages; 

    /** Mutual exclusive lock for memory allocation. */
    private static Lock lock;

    /** Condition to wait for memory to be freed. */
    private static Condition condition;

    /** 
     * In a multiCPU scenario, this spinlock will guarantee that 
     * only one CPU at the time will be able to allocate memory. */
    private static SpinLock spinlock;

    /**
     * This method needs to be called before any memory allocation.
     * It initializes all the necessary fields. */
    public static void init() {
	// If free pages is null (which will only happen before calling this
	// method, all the other fields will also be null. Therefore, it is
	// assumed that every field has to be initialized.
	if (freePages == null) {
	    
	    freePages = new ArrayList<>();
	    
	    for (int i = 0; i < Machine.NumPhysPages; i++)
		freePages.add(i);
	    

	    // It creates a new lock, condition, and spinlock.
	    lock = new Lock("MemoryAllocator lock");
	    condition = new Condition("MemoryAllocator condition", lock);
	    spinlock = new SpinLock("MemoryAllocator spinlock");
	}
    }

    /**
     * This method allocates the number of pages asked for. If there
     * is no enough memory at that time, it waits until some memory 
     * is freed. 
     * 
     * @param npages Number of pages are to be allocated.
     * @return An int array with the id of the allocated pages.
     * */
    public static int[] allocatePages(int npages) {
	
	// Array of pages to be returned 
	int[] pages;

	// Block other threads to allocate/free memory
	lock.acquire();

	// Prevent other CPUs of allocating/freeing memory at the same time
	spinlock.acquire();

	// Wait while there is no enough memory to be allocated
	while (freePages.size() < npages) {
	    // Release spinlock
	    spinlock.release();

	    // Go to sleep until some memory is 
	    // dealocated
	    condition.await();

	    // Re-acquire the spinlock
	    spinlock.acquire();
	}

	
	// Now, memory is available 
	pages = new int[npages];
	
	for (int i = 0; i < pages.length; i++) 
	    // this line takes from freePages one page and push it into occupiedPages.
	    // Also, a copy of this page is given to pages[i], so it can be returned.
	    pages[i] = freePages.remove(freePages.size() - 1);

	
	// releases the lock and the spinlock
	lock.release();	
	spinlock.release();

	// return point.
	return pages;
    }

    /**
     * This method allocates one page. If there is no enough memory
     * at that time, it waits until some memory is freed. 

     * @return Id for the page allocated.
     * */
    public static int allocatePage() {
	int page;
	// Block other threads to allocate/free memory
	lock.acquire();

	// Prevent other CPUs of allocating/freeing memory at the same time
	spinlock.acquire();
	
	while (freePages.isEmpty()) {
	    // Release spinlock
	    spinlock.release();

	    // Go to sleep until some memory is 
	    // dealocated
	    condition.await();

	    // Re-acquire the spinlock
	    spinlock.acquire();
	}
	
	// Now, there is a page available
	// this line takes from freePages one page and push it into occupiedPages.
	page = freePages.remove(freePages.size() - 1);
		

	
	// releases the lock and the spinlock
	lock.release();	
	spinlock.release();
	
	// return point
	return page;
    }
    
    /**
     * Deallocate some allocated page.
     * 
     * @param npage Id of page to be freed
     * */
    public static void free(int npage) {
	// Block other threads to allocate/free memory
	lock.acquire();

	// Prevent other CPUs of allocating/freeing memory at the same time
	spinlock.acquire();

	// Now, the page is put back into the freePages stack.
	freePages.add(npage);
	
	
	
	// If there's any thread waiting for memory, 
	// wakes them up
	condition.broadcast();
	
	// releases the lock and the spinlock
	lock.release();	
	spinlock.release();
    }
    
    /**
     * Deallocate an array of allocated pages.
     * 
     * @param npages array of Ids od pages to be freed.
     * */
    public static void free(int[] npages) {
	// Block other threads to allocate/free memory
	lock.acquire();

	// Prevent other CPUs of allocating/freeing memory at the same time
	spinlock.acquire();
	for (int index = 0; index < npages.length; index++) {
	    // As a matter of simplicity and performance, instead of calling the
	    // method responsible for freeing only one page, it can be done
	    // right here while we are holding the lock and spinlock.
	    // Free page is now put back into freePages stack
	    freePages.add(npages[index]);
	}



	// If there's any thread waiting for memory, 
	// wakes them up
	condition.broadcast();

	// releases the lock and the spinlock
	lock.release();	
	spinlock.release();
    }
}


