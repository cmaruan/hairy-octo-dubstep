/* provided by professor */
package nachos.util;

import java.util.ArrayList;
import nachos.kernel.threads.SpinLock;
import nachos.machine.CPU;
import nachos.machine.InterruptHandler;
import nachos.machine.Timer;

/**
 * This class "explodes" a single hardware timer and makes it look
 * like multiple independent hardware timers.
 * 
 * @author E. Stark
 * @version 20141002
 */

public class TimerService {
    
    /** The underlying hardware timer. */
    private Timer timer;
    
    /**
     * Interrupt handler for underlying hardware timer, or null if the
     * timer is currently stopped.
     */
    private InterruptHandler mainHandler;
    
    /** List of registered interrupt handlers. */
    private ArrayList<InterruptHandler> handlers;
    
    /** Spin lock to exclude an interrupt handler running on another CPU. */
    private SpinLock spinlock;
    
    /**
     * Initialize a TimerService using a specified underlying hardware timer.
     * 
     * @param timer
     */
    public TimerService(Timer timer) {
	this.timer = timer;
	handlers = new ArrayList<InterruptHandler>();
	spinlock = new SpinLock("Timer service spinlock for " + timer);
    }
    
    /**
     * Get the resolution of this timer service.
     * 
     * @return  The time interval, in ticks, between interrupts generated by the
     * underlying hardware timer.
     */
    public int getResolution() {
	return timer.interval;
    }
     
    /**
     * Invoke all handlers registered at the time of the most recent timer interrupt.
     * Called from mainHandler, where interrupts will be disabled, but no spin lock held.
     */
    private void invokeHandlers() {
	// Exclude threads on other CPUs trying to manipulate the handler list.
	spinlock.acquire();
	
	// With exclusion held, take a snapshot of the current handler list.
	ArrayList<InterruptHandler> hl = (ArrayList<InterruptHandler>) handlers.clone();
	
	// We don't want to hold the spin lock while invoking handlers, because we
	// don't know how long they might take to run.  If a handler is added by a thread
	// on another CPU after we release this lock, it will not be in the snapshot
	// and will not be invoked until the next timer interrupt.  If a handler in the
	// snapshot is removed from the handler list by a thread on another CPU after
	// we release this lock, then the handler will get invoked even though it has
	// been removed.  Handlers must be able to tolerate this situation.
	spinlock.release();
	
	// Now invoke the handlers in the snapshot.  Interrupts are disabled, but
	// we are not holding the spin lock.
	for(InterruptHandler handler : hl)
	    handler.handleInterrupt();
    }

    /**
     * Register an interrupt handler.
     * 
     * @param handler the handler to register.
     */
    public void addHandler(InterruptHandler handler) {
	// Prevent interrupts on this CPU
	int oldLevel = CPU.setLevel(CPU.IntOff);
	
	// Exclude interrupt handlers on other CPUs.
	spinlock.acquire();
	
	// Now safe to add handler to list of registered handlers.
	handlers.add(handler);
	if(mainHandler == null) {
	    mainHandler =
		    new InterruptHandler() {
			@Override
			public void handleInterrupt() {
			    invokeHandlers();
			}
	    	    };
	    timer.setHandler(mainHandler);
	    timer.start();
	}
	
	// Unlock stuff before returning
	spinlock.release();
	CPU.setLevel(oldLevel);
    }
    
    /**
     * Unregister an interrupt handler.
     * It is possible, under certain conditions, for a handler to be invoked
     * on one CPU, even after it has been removed by a thread running on another CPU.
     * Handlers must be designed to tolerate such an occurrence.
     * 
     * @param handler the handler to unregister.
     */
    public void removeHandler(InterruptHandler handler) {
	// Prevent interrupts on this CPU
	int oldLevel = CPU.setLevel(CPU.IntOff);
	
	// Exclude interrupt handlers on other CPUs.
	spinlock.acquire();
	
	// Now save to remove handler from list of registered handlers.
	handlers.remove(handler);
	if(handlers.size() == 0) {
	    mainHandler = null;
	    timer.stop();
	}
	
	// Unlock stuff before returning
	spinlock.release();
	CPU.setLevel(oldLevel);
   }
}
