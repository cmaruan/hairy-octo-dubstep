"Calvin: Isn�t it strange that evolution would give us a sense of humour? When you think about it, it�s weird that we have a physiological response to absurdity. We laugh at nonsense. We like it. We think it�s funny. Don�t you think it�s odd that we appreciate absurdity? Why would we develop that way? How does it benefit us?
Hobbes: I suppose if we couldn�t laugh at things that don�t make sense, we couldn�t react to a lot of life.
Calvin: (after a long pause) I can�t tell if that�s funny or really scary."
link: http://karmajello.com/postcont/2014/01/Calvin-and-Hobbes-Sense-of-Humor.jpg