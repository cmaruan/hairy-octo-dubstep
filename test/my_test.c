#include "syscall.h"

/* Testing program */

int test_exec() {
	char *args[] = {
			"one", "two", "three"
	};
	Exec("crap program",  args);
	return 0;
}

/**
 * This program only tests if the return
 * value is being captured by the operating
 * system. */
int test_return(int return_value) {
	Exit(return_value);

	/* I should not execute here */
	return 0;
}

int main() {
	test_exec();
	return 0;
}
